const router = require("express").Router()
const user = require('./controllers/userController')
const auth = require('./controllers/authController')

router.get('/',user.home)
router.get('/login',user.login)
router.get('/dashboard',user.dashboard)
router.get('/game',user.game)
router.post('/login',auth.login)

router.get("/dashboard/users/form", (req, res) => {
    res.render("users/form", { id: null })
})

// form edit
router.get("/dashboard/users/form/:id", (req, res) => {
    res.render("users/form", { id: req.params.id })
})

// Detail view
router.get("/dashboard/users/:id", (req, res) => {
    res.render("users/detail", {
        id: req.params.id
    })
})
module.exports = router