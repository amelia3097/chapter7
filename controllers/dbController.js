const { UserGame, UserGameBiodata, UserGameHistory } = require("../models")
module.exports = {
    readByUsername: async (username) => {
        let response = {}
        try {
            response = await UserGame.findOne({
                where: { username: username },
                include: [
                    {
                        model: UserGameBiodata,
                        as: "usergamebiodata",
                        // required: true
                    }
                ]

            })
        } catch (e) {
            console.log(e.message)
            
        }
        finally{
            return response

        }
    }
}