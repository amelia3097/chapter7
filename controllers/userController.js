module.exports = {
    home: (req, res) => {
        res.sendFile(path.join(__dirname+'/public/index.html'))
    },
    dashboard: (req, res) => {
        res.render("users/list")
    },
    login: (req, res) => {
        res.render("login")
    },
    game: (req, res) => {
        res.sendFile(path.join(__dirname+'/public/chpter4.html'))
    },
}
