
const jwt = require('jsonwebtoken')
const db = require('./dbController')
module.exports = { 
    login: async (req, res) => {
        let response = {}
        try {
            let userData = await db.readByUsername(req.body.username)
            if (userData) {
                if (req.body.password == userData.password) {
                    let userDataSent = {}
                    userDataSent.id = userData.id
                    userDataSent.username = userData.username
                    userDataSent.token = jwt.sign(userDataSent, 's3cr3t')

                    response = { ...userDataSent }
                    response.success = true
                }
            }
            else {
                res.status(401)
               
             response.success = false

            }
        } catch (e) {
            res.status(401)
           
            response.success = false
        }
        finally {
            res.json(response)
        }
    }
    }
