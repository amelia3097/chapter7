const { render } = require("ejs")
const express = require("express")
const app = express()
const users = require("./api/users")
const path = require('path')
const router = require('./router')
const http = require("http")
const server = http.createServer(app)
const SocketIO = require("socket.io").Server

const io = new SocketIO(server)


app.use(express.static(path.join(__dirname,'./public')))

app.set("view engine", "ejs")

app.use("/api/v1", users)

app.use(express.json())

app.use(express.urlencoded({extended: true}))

app.use(router)

let player = []


io.on("connection", (socket) => {
    console.log(`new websocket connection id: ${socket.id}`)
    
    socket.on("register", (data) => {
        player.push(data.username)
       
            console.log(data)
            // kirim ke user lain
            socket.broadcast.emit(`room`, player)
    
            // kirim ke sender
            socket.emit(`room`, player)
        
    })


    // ...
    socket.on("game", (data) => {
        if (data && data.room && data.text) {
            console.log(data)
            player.push(data.username)
            // kirim ke user lain
            socket.broadcast.emit(`game.${data.room}`, data.text)
    
            // kirim ke sender
            socket.emit(`notif.${data.room}`, "choice sent!")
        }
    })

    socket.on("disconnect", () => {
        // player = []
        console.log(`client disconnect: ${socket.id}`)
    })
})

// form create


server.listen(8000, () => console.log(`Server running on port 8000`))