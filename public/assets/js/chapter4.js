let user = JSON.parse(window.localStorage.getItem('user'))

if(!user) {
    window.location.replace('http://localhost:8000/login')
}
const WARNA_SHADOW = '#C4C4C4'
const WARNA_TRANSPARANT = '#c4c4c400'

var isClicked = false

var playerchoose
var comchoose





document.getElementById('insideBatu').addEventListener('mouseover', function () {
    if (isClicked == false) {
        document.getElementById('insideBatu').style.backgroundColor = WARNA_SHADOW
    }

})
document.getElementById('insideBatu').addEventListener('mouseout', function () {
    if (isClicked == false) {
        document.getElementById('insideBatu').style.backgroundColor = WARNA_TRANSPARANT
    }

})

document.getElementById('insideKertas').addEventListener('mouseover', function () {
    if (isClicked == false) {

        document.getElementById('insideKertas').style.backgroundColor = WARNA_SHADOW
    }
})
document.getElementById('insideKertas').addEventListener('mouseout', function () {
    if (isClicked == false) {

        document.getElementById('insideKertas').style.backgroundColor = WARNA_TRANSPARANT
    }

})

document.getElementById('insideGunting').addEventListener('mouseover', function () {
    if (isClicked == false) {

        document.getElementById('insideGunting').style.backgroundColor = WARNA_SHADOW
    }
})
document.getElementById('insideGunting').addEventListener('mouseout', function () {
    if (isClicked == false) {

        document.getElementById('insideGunting').style.backgroundColor = WARNA_TRANSPARANT
    }
})



function batuPlayer() {
    if (isClicked == false) {
        choose('batu')
        clickedAction()


    }

}

function kertasPlayer() {
    if (isClicked == false) {
        choose('kertas')
        clickedAction()

    }
}

function guntingPlayer() {
    if (isClicked == false) {
        choose('gunting')
        clickedAction()
    }
}

function refresh() {
    location.reload()
}


function clickedAction() {
    isClicked = true
    if(!comchoose) {
        modal.show()
    }


}

function fight (player1, player2) {
    if (player1 == "batu") {
        if (player2 == "batu") {
            document.getElementById('shadowbatu').style.backgroundColor = WARNA_SHADOW
            return "DRAW"
        }
        else if (player2 == "kertas") {
            document.getElementById('shadowkertas').style.backgroundColor = WARNA_SHADOW
            return "LOSE"
        }
        else if (player2 == "gunting") {
            document.getElementById('shadowgunting').style.backgroundColor = WARNA_SHADOW
            return "WIN"
        }
    }
    else if (player1 == "kertas") {
        if (player2 == "batu") {
            document.getElementById('shadowbatu').style.backgroundColor = WARNA_SHADOW
            return "WIN"
        }
        else if (player2 == "kertas") {
            document.getElementById('shadowkertas').style.backgroundColor = WARNA_SHADOW
            return "DRAW"
        }
        else if (player2 == "gunting") {
            document.getElementById('shadowgunting').style.backgroundColor = WARNA_SHADOW
            return "LOSE"
        }
    }
    else if (player1 == "gunting") {
        if (player2 == "batu") {
            document.getElementById('shadowbatu').style.backgroundColor = WARNA_SHADOW
            return "LOSE"
        }
        else if (player2 == "kertas") {
            document.getElementById('shadowkertas').style.backgroundColor = WARNA_SHADOW
            return "WIN"
        }
        else if (player2 == "gunting") {
            document.getElementById('shadowgunting').style.backgroundColor = WARNA_SHADOW
            return "DRAW"
        }
    }
}


function battle() {
    let result = fight(playerchoose,comchoose)
    console.log(`p1 = ${playerchoose}`)
    console.log(`p2 = ${comchoose}`)
    console.log(`result = ${result}`)
    if (result == 'WIN') {
        document.getElementById('results').innerHTML = 'WIN'
        document.getElementById('versus').style.display = 'none'
        document.getElementById('verwin').style.display = 'flex'
        

    }
    if (result == 'LOSE') {
        document.getElementById('results').innerHTML = 'LOSE'
        document.getElementById('versus').style.display = 'none'
        document.getElementById('verwin').style.display = 'flex'

    }

    if (result == 'DRAW') {
        document.getElementById('results').innerHTML = 'DRAW'
        document.getElementById('versus').style.display = 'none'
        document.getElementById('verwin').style.display = 'flex'

    }
  
}
    

        let socket = io("http://localhost:8000")
        let content = document.getElementById("content")
        let message = document.getElementById("message")
        let userSelect = document.getElementById("users")
        const modal = new bootstrap.Modal(document.querySelector(".modal"), {})
        modal.show()

        let room = "public" 
        try {
            room = location.search.replace("?room=", "")
            if (!room) {
                window.location.href = "?room=public"
            }
        } catch(e) {
            console.log(e)
        }

        socket.on("connect", () => {
            console.log("connected!")
        })

        socket.on(`room`, (data) => {
            console.log(data)
            if(data.length >= 2) {
                modal.hide()
                console.log(`p1 = ${data[0]}`)
                console.log(`p2 = ${data[1]}`)
            }
        })

        // diterima oleh sender
        socket.on(`notif.${room}`, (data) => {
            console.log(`notif = ${data}`)
            if(comchoose) {
                battle()
            }
        })

        // diterima oleh user lain
        socket.on(`game.${room}`, (data) => {
            modal.hide()
            comchoose = data
            console.log(`enemychoose = ${data}`)
            if(playerchoose) {
                battle()
            }
        })

        function choose (choice) {
            playerchoose = choice
            console.log(`choose ${choice}`)
            socket.emit("game", {
                text: choice,
                room: room
            })
        }
    
        socket.emit("register", {
           username: user.username
        })

